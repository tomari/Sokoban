A Sokoban clone with stage editor for Android devices.
Requires Android 3.0+

[![pipeline status](https://gitlab.com/tomari/Sokoban/badges/master/pipeline.svg)](https://gitlab.com/tomari/Sokoban/commits/master)

## APK downloads

* [Amazon App Store](https://www.amazon.com/gp/product/B00INTVBQW/ref=mas_pm_Dr-Soko)
* [GitLab Releases](https://gitlab.com/tomari/Sokoban/-/releases)

## Compile
Use Android Studio

## License
GPLv3 (See COPYING)

